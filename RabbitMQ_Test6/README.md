#Remote Procedure Call

![](rpc.png)

##rpc_server.js
- **Opens connection to RabbitMQ server**
- **Opens channel**
- **Declares queue**
- **Spreads work load to multiple servers (prefetch1)**
- **Consumes from queue, doeas the job(fibonacci function) and replies**
- **Gets correlationId from queue and sets it when replying**

##rpc_client.js
- **Checks arguments that client is sending**
- **Opens connection to RabbitMQ server**
- **Opens channel**
- **Creates queue with server generated name (exclusive, once closed queue is deleted)**
- **Creates random correlationID (generateUuid)**
- **Parses number from user args**
- **Consumes from queue, checks message properties for correlationID**
- **Closes connection**
- **Sends to queue, sets correlationId and replyTo queue name**

**(CTRL+c to close/exit and delete queue)**

