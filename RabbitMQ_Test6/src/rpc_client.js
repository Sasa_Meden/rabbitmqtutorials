#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

var args = process.argv.slice(2);
// console.log('ARGS: ' + args);
if (args.length == 0) {
  console.log('Usage: rpc_client.js num');
  process.exit(1);
}

amqp.connect('amqp://localhost', (err, conn) => {
  conn.createChannel((err, ch) => {
        
    // with empty string in assertQueue function server will generate random que name (q.queue)
    // that queue is nondurable so when consumer disconnects queue is deleted (exclusive flag) 
    ch.assertQueue('', {exclusive: true}, (err,q) => {
      var corr = generateUuid();// correlation
      var number = parseInt(args[0]);
      console.log(' [x] Requesting fib(%d)', number);
             
      ch.consume(q.queue, (msg) => {
        if(msg.properties.correlationId == corr) {
          console.log(' [.] Got %s', msg.content.toString());
          setTimeout(function() { conn.close(); process.exit(0) }, 500);
        }
      }, {noAck: true});
      ch.sendToQueue('rpc_queue', new Buffer(number.toString()),
      { correlationId: corr, replyTo: q.queue });
    });
  });
});

generateUuid = () => {
  return Math.random().toString() +
         Math.random().toString() +
         Math.random().toString();
}