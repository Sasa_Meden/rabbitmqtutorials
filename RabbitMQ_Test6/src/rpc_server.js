#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', (err, conn) => {
  conn.createChannel((err, ch) => {
    var myQ = 'rpc_queue'; // Name queue (I'm using default queue for now) 
    
    ch.assertQueue(myQ, {durable: false});
    ch.prefetch(1);// spread the load equally over multiple servers
    console.log(' [x] Awaiting RPC requests');
    ch.consume(myQ, reply = (msg) => {
      var number = parseInt(msg.content.toString());
      console.log(" [.] fib(%d)", number);
      var rply = fibonacci(number);
     
      ch.sendToQueue(msg.properties.replyTo,
         new Buffer(rply.toString()),
         {correlationId: msg.properties.correlationId});
      ch.ack(msg);
    });
  });
});

fibonacci = (number) => {
  if (number == 0 || number == 1 || number > 45) // last one is just to cover overworking ()
    return number;
  else
    return fibonacci(number - 1) + fibonacci(number - 2);
}