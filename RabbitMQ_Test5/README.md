#Topic Exchange

![](topic_exchange.png)

##emit_log_topic.js
- **Opens connection to RabbitMQ server**
- **Opens channel**
- **Declares exchange with name and topic type**
- **Declares key if args > 0 key = args[0] else key ='anonymous.info'**
- **Publishes message to named exchange with key(s)**
- **Closes connection**

##receive_logs_topic.js
- **Opens connection to RabbitMQ server**
- **Opens channel**
- **Declares exchange (same name) and sets type(topic), durable=false**
- **Creates queue with server generated name (exclusive, once closed queue is deleted)**
- **Binds that queue with our exchange for each key**
- **Consumes (recieves message from that queue)**

**(CTRL+c to close/exit and delete queue)**
**To receive all the logs: console:  receive_logs_topic.js '#'**
**To receive all logs from the facility 'kern': console: receive_logs_topic.js 'kern.*'**
**To receive only critical logs: console: receive_logs_topic.js '*.critical'**
**When a queue is bound with '#' (hash) binding key - it will receive all the messages, regardless of the routing key - like in fanout exchange.**
**To emit log with routing key 'kern.critical': console:  emit_log_topic.js 'kern.critical' 'Some message'**
**You can create multiple bindings: receive_logs_topic.js "kern.*" "*.critical"**
**Order of keys is important**
