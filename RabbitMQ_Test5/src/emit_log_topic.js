#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', (err, conn) => {
  conn.createChannel((err, ch) => {
    var exchange = 'topic_logs'; // Name exchange 
    var args = process.argv.slice(2);
  //  console.log('ARGS: ' + args);
    var key = (args.length > 0) ? args[0] : 'anonymous.info';
  //  console.log('KEY: ' + key);
    var msg = args.slice(1).join(' ') || 'Some message text';
  //  console.log('MSG: ' + msg);
    // Create exchange named topic_logs, type topic 
    // send to all queues that have certain topic that they are interested in
    ch.assertExchange(exchange, 'topic', {durable: false}); 
    // second param - key: routing key, so receiving script can select topic it's set up for
    ch.publish(exchange, key, new Buffer(msg)); 
    console.log(" [x] Sent %s: '%s'", key, msg);
  });
  setTimeout(() => { conn.close(); process.exit(0) }, 500);
});