#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

var args = process.argv.slice(2);
// console.log('ARGS: ' + args);
if (args.length == 0) {
  console.log('Usage: receive_logs_topic.js <facility>.<severity>');
  process.exit(1);
}

amqp.connect('amqp://localhost', (err, conn) => {
  conn.createChannel((err, ch) => {
    var exchange = 'topic_logs'; // Name exchange 
    
    ch.assertExchange(exchange, 'topic', {durable: false});
    // with empty string in assertQueue function server will generate random que name (q.queue)
    // that queue is nondurable so when consumer disconnects queue is deleted (exclusive flag) 
    ch.assertQueue('', {exclusive: true}, (err,q) => {
      console.log(' [*] Waiting for logs. To exit press CTRL+C');
      // we are telling our exchange to send message to our queue (binding), including severity type
      args.forEach((key) => {
        ch.bindQueue(q.queue, exchange, key);
      });
       
      ch.consume(q.queue, (msg) => {
        console.log(" [x] Sent %s: '%s'", msg.fields.routingKey, msg.content.toString());
      }, {noAck: true});
    });
  });
});

// To receive all the logs: console:  receive_logs_topic.js '#'
// To receive all logs from the facility 'kern': console: receive_logs_topic.js 'kern.*'
// To receive only critical logs: console: receive_logs_topic.js '*.critical'
// To emit log with routing key 'kern.critical': console:  emit_log_topic.js 'kern.critical' 'A critical kernel error'
// When a queue is bound with '#' (hash) binding key - it will receive all the messages,
// regardless of the routing key - like in fanout exchange.
// You can create multiple bindings: receive_logs_topic.js "kern.*" "*.critical"
// order of emmit and receive is important