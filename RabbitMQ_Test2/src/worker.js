#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', (err, conn) => {
  conn.createChannel((err, ch) => {
    var myQ = 'taskQue';
    
    ch.assertQueue(myQ, {durable: true});
    ch.prefetch(1);
    console.log(' [*] Waiting for messages in %s. To exit press CTRL+C', myQ);
    
    ch.consume(myQ, (msg) => {
      var seconds = msg.content.toString().split('.').length - 1;
      
      console.log(' [x] Received %s', msg.content.toString());
      setTimeout(() => {
        console.log(' [x] Done');
        ch.ack(msg);
      }, seconds * 1000);
    }, {noAck: false});
  });
});