#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', (err, conn) => {
  conn.createChannel((err, ch) => {
    var myQ = 'taskQue';
    var msg = process.argv.slice(2).join(' ') || 'Message text';

    ch.assertQueue(myQ, {durable: true});
    // Note: on Node 6 Buffer.from(msg) should be used
    ch.sendToQueue(myQ, new Buffer(msg), {persistent: true});
    console.log(' [x] Sent %s', msg);
  });
  setTimeout(() => { conn.close(); process.exit(0) }, 500);
});