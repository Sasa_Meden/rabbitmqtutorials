#Second test - simulate some work, check if caching of tasks works

![](workers_calculators.png)

##new_task.js
- **Opens connection to RabbitMQ server**
- **Opens channel**
- **Declares a queue and sets it to durable**
- **Sends message with some params (No. of "." behind message represents No. of timeout seconds)**
- **Closes connection**

##worker.js
- **Opens connection to RabbitMQ server**
- **Opens channel**
- **Declares a queue (same name) and sets it to durable**
- **Provides callback to receive message/task**
- **Makes queue durable (queue won't be lost if server restarts)**
- **Makes message persistent (tells RabbitMQ to save it to disk or cache)**
- **Makes shure that it does not give worker(consumer) more then one message at a time**
- **Makes shure that it acknowledges messages so if worker dies nothing will be lost (all unacknowledged messages will be redelivered)**
- **Does the task (in this case timeout No. of seconds received in message)**

**(CTRL+c to close/exit queue)**

