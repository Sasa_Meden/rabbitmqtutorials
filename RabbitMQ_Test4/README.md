#Publish/Subscribe - Routing

![](publish_subscribe_routing.png)

##emit_log_direct.js
- **Opens connection to RabbitMQ server**
- **Opens channel**
- **Declares exchange with name and direct type**
- **Declares severity if args > 0 severity = args[0] else severity ='info'**
- **Publishes message to named exchange with severity**
- **Closes connection**

##receive_logs_direct.js
- **Opens connection to RabbitMQ server**
- **Opens channel**
- **Declares exchange (same name) and sets type(direct), durable=false**
- **Creates queue with server generated name (exclusive, once closed queue is deleted)**
- **Binds that queue with our exchange for each severity**
- **Consumes (recieves message from that queue)**

**(CTRL+c to close/exit and delete queue)**
**To save logs: console:  receive_logs_direct.js info > logs_from_rabbit.log - if you just want to save info**
**Call receive_logs_direct.js info  with certain type of severity you want to receive (in this case 'info')**
**To see all bindings: console: sudo rabbitmqctl list_bindings**
**To emit message with certain severity: console:  emit_log_direct.js error 'Some error message'**
