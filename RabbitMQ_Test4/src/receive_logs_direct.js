#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

var args = process.argv.slice(2);

if (args.length == 0) {
  console.log("Usage: receive_logs_direct.js [info] [warning] [error]");
  process.exit(1);
}

amqp.connect('amqp://localhost', (err, conn) => {
  conn.createChannel((err, ch) => {
    var exchange = 'direct_logs'; // Name exchange 
    
    ch.assertExchange(exchange, 'direct', {durable: false});
    // with empty string in assertQueue function server will generate random que name (q.queue)
    // that queue is nondurable so when consumer disconnects queue is deleted (exclusive flag) 
    ch.assertQueue('', {exclusive: true}, (err,q) => {
      console.log(' [*] Waiting for logs. To exit press CTRL+C');
      // we are telling our exchange to send message to our queue (binding), including severity type
      args.forEach((severity) => {
        ch.bindQueue(q.queue, exchange, severity);
      });
       
      ch.consume(q.queue, (msg) => {
        console.log(" [x] %s: '%s'", msg.fields.routingKey, msg.content.toString());
      }, {noAck: true});
    });
  });
});

// to save logs: console:  receive_logs_direct.js info > logs_from_rabbit.log - if you just want to save info
// call receive_logs_direct.js info  with certain type of severity you want to receive
// to see all bindings: console: sudo rabbitmqctl list_bindings
// to emit certain log message: console:  emit_log_direct.js error "Some error message"
