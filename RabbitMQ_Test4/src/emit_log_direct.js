#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', (err, conn) => {
  conn.createChannel((err, ch) => {
    var exchange = 'direct_logs'; // Name exchange 
    var args = process.argv.slice(2);
    var msg = process.argv.slice(2).join(' ') || 'Some message text';
    // check args length and set severity
    var severity = (args.length > 0) ? args[0] : 'info';
    // Create exchange named logs, type direct = send to all queues that have certain binding key
    ch.assertExchange(exchange, 'direct', {durable: false}); 
    // second param - severity: routing key, so receiving script can select severity it wants to receive
    ch.publish(exchange, severity, new Buffer(msg)); 
    console.log(" [x] Sent %s: '%s'", severity, msg);
  });
  setTimeout(() => { conn.close(); process.exit(0) }, 500);
});