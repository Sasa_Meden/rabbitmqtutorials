#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

var message_sender = 'sasa.meden@codecons.com';
var message_receiver= 'sasa.meden@gmail.com';
var message_text = 'Some email text';

var key = 'email';

amqp.connect('amqp://localhost', (err, conn) => {

if(err != null){
  console.log(err.syscall + ': ' + err.code);
  process.exit(1);
  // maybe try to start server? or admin does that?
}

  conn.createChannel((err, ch) => {
    
    if(err != null){
     console.log(err);
     conn.close();
     process.exit(0);
    }  
    
    var exchange = getExchange('email_logs'); // Name exchange 
    var msg = getMessage(message_sender, message_receiver, message_text);
    var severity = getSeverity(key);
    try{
      ch.publish(exchange, severity, new Buffer(JSON.stringify(msg))); 
    }catch(error){
      console.log(err);
      conn.close();
      process.exit(0);
    }
    
    console.log(" [x] Sent %s: '%s'", severity, JSON.stringify(msg));
  });
  setTimeout(() => { conn.close(); process.exit(0) }, 500);
});

getExchange = (exch) => {
  return exch;
}
getSeverity = (sev) => {
  return sev;
}
getMessage = (sender,receiver,text) => {
  var eMail = {
    sender_email: sender,
    receiver_email: receiver,
    message_email: text
  }; 
  return eMail;
}