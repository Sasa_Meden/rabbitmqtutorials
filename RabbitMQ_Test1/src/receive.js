#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', (err, conn) => {
  conn.createChannel((err, ch) => {
    var myQ = 'testQue';

    ch.assertQueue(myQ, {durable: false});
    console.log(' [*] Waiting for messages in %s. To exit press CTRL+C', myQ);
    ch.consume(myQ, (msg) => {
      console.log(' [x] Received %s', msg.content.toString());
    }, {noAck: true});
  });
});