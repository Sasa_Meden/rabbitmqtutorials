#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', (err, conn) => {
  conn.createChannel((err, ch) => {
    var myQ = 'testQue';
    var msg = 'First message';

    ch.assertQueue(myQ, {durable: false});
    // Note: on Node 6 Buffer.from(msg) should be used
    ch.sendToQueue(myQ, new Buffer(msg));
    console.log(' [x] Sent %s', msg);
  });
  setTimeout(() => { conn.close(); process.exit(0) }, 500);
});