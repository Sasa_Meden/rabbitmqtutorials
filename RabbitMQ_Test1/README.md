#First test send/recieve

![](simple_message.png)

##send.js
- **Opens connection to RabbitMQ server**
- **Opens channel**
- **Declares a queue**
- **Sends a message**
- **Closes connection**

##receive.js
- **Opens connection to RabbitMQ server**
- **Opens channel**
- **Declares a queue (same name)**
- **Provides callback to receive message**

**(CTRL+c to close/exit queue)**

