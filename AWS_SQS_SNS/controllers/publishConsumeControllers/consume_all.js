const AWS = require('aws-sdk');
// const util = require('util');
const config = require('../../config.json');

// configure AWS
AWS.config.update({region: 'us-east-2'});

const sqs = new AWS.SQS();

const receiveMessageParams = {
  QueueUrl: config.QueueUrl,
  MaxNumberOfMessages: 10,
  VisibilityTimeout: 1000, // This is how much time message will be in flight(not available to other consumers while being processed)
};
let i = 0;

function getMessages() {
  sqs.receiveMessage(receiveMessageParams, receiveMessageCallback);
}

function receiveMessageCallback(err, data) {
  if (data && data.Messages && data.Messages.length > 0) {
    for (i = 0; i < data.Messages.length; i++) {
      console.log(JSON.parse(data.Messages[i].Body).Message);
      // Delete the message when we've successfully processed it

      const deleteMessageParams = {
        QueueUrl: config.QueueUrl,
        ReceiptHandle: data.Messages[i].ReceiptHandle,
      };

      sqs.deleteMessage(deleteMessageParams, deleteMessageCallback);
    }
    getMessages();
  } else {
    // process.exit(1);
    setTimeout(getMessages, 1000);
  }
}
function deleteMessageCallback(err, data) {
  // console.log("deleted message");
  // console.log(data);
}

setTimeout(getMessages, 1000);
