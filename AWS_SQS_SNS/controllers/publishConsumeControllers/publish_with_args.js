const AWS = require('aws-sdk');
const config = require('../../config.json');

require('dotenv').config();
// configure AWS
AWS.config.update({region: 'us-east-2'});

const sns = new AWS.SNS();
let i = 0;

function publish(mesg) {
  const publishParams = {
    TopicArn: config.TopicArn,
    Message: mesg,
    MessageAttributes: {
      Person: {
        DataType: 'String',
        StringValue: 'Steve',
      },
    },
  };

  sns.publish(publishParams, (err, data) => {
    console.log('sent: ' + publishParams.Message + '\n' + JSON.stringify(publishParams.MessageAttributes) + '\n');
  });
}
// setTimeout(publish, 1000);
for (i = 0; i < 10; i++) {
  publish('Args Message ' + i);
}
