const AWS = require('aws-sdk');
// const util = require('util');
const fs = require('fs');
// const cmd = require('node-cmd');

// configure AWS
AWS.config.update({region: 'us-east-2'});

const sns = new AWS.SNS();
const sqs = new AWS.SQS();

const config = {};
// const queueName = 'standard_queue1';
// const awsQueues = [];
let noQueue = true;
// const paramsGetQueueUrl = {
//   QueueName: '',
// };
let i = 0;

config.TopicArn = 'arn:aws:sns:us-east-2:719738972379:message_topic';// Do we want this in file?

/**
 * List ALL Topics
 * @returns String Array of TopicArns
 * rejects with error
 */

function listTopics() {
  const topicsArn = [];

  return new Promise((resolve, reject) => {
    sns.listTopics({}, (err, result) => {
      if (err) {
        reject(err);
      } else {
        for (i = 0; i < result.Topics.length; i++) {
          topicsArn[i] = result.Topics[i].TopicArn;
        }
        //console.log(topicsArn);
        resolve(topicsArn);
      }
    });
  });
}
/**
 * List ALL queues from AWS SQS
 * @returns null if no queues or String Array of queueu URL's
 * rejects with error
 */

function listAllQueues() {
  return new Promise((resolve, reject) => {
    sqs.listQueues({}, (err, result) => {
      if (err) {
        reject(err);
      } else if (result.QueueUrls.length < 1) {
        resolve(null);
      } else {
        console.log(result.QueueUrls);
        resolve(result.QueueUrls);
      }
    });
  });
}

/**
 * Check if queue with certain name exists
 * @param awsQueues String Array of existing queues
 * @param queueName queue name (String) we are searching for
 * @returns false if no queue, true if queue exists
 */

function checkIfQueueExists(awsQueues, queueName) {
  noQueue = true;
  return new Promise(resolve => {
    for (i = 0; i < awsQueues.length; i++) {
      if (awsQueues[i].indexOf(queueName) !== -1) {
        noQueue = false;
        resolve(noQueue);
      }
    }
    if (noQueue) {
      resolve(noQueue);
    }
  });
}

/**
 * Create Queue with certain name
 * @param queueName queue name (String) we are creating
 * @returns cretated Queue URL
 * rejects with error
 */

function createQueue(queueName) {
  return new Promise((resolve, reject) => {
    sqs.createQueue({QueueName: queueName}, (err, result) => {
      if (err) {
        reject(err);
      } else {
        config.QueueUrl = result.QueueUrl; // Do we want this in file?
        resolve(result.QueueUrl);
      }
    });
  });
}

/**
 * Create Queue with certain name
 * @param queueUrl queue url (String)
 * @returns QueueArn
 * rejects with error
 */

function getQueueAttributes(queueUrl) {
  return new Promise((resolve, reject) => {
    sqs.getQueueAttributes({QueueUrl: queueUrl, AttributeNames: ['QueueArn'] }, (err, result) => {
      if (err) {
        reject(err);
      } else {
        config.QueueArn = result.Attributes.QueueArn;// Do we want this in file?
        resolve(result.Attributes.QueueArn);
      }
    });
  });
}

/**
 * Subscibe Queue  to certain topic
 * @param topicArn TopicArn we are subscribing queue to
 * @param protocol used protocol ('sqs')
 * @param queueArn QueueArn from queue attributes (endpoint)
 * @returns true if subscription passed
 * rejects with error
 */

function snsSubscribe(topicArn, protocol, queueArn) {
  return new Promise((resolve, reject) => {
    sns.subscribe({TopicArn: topicArn, Protocol: protocol, Endpoint: queueArn }, (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(true);
      }
    });
  });
}

/**
 * set Queue attributes
 * @param queueUrl queue url (String)
 * @param topicArn topicARN queue is subscribed to
 * @param queueArn queueArn used to tie it to topic
 * @returns true if attributes are set
 * rejects with error
 */

function setQueueAttributes(queueUrl, topicArn, queueArn) {
  const attributes = {
    Version: '2008-10-17',
    Id: queueArn + '/SQSDefaultPolicy',
    Statement: [{
      Sid: 'Sid' + new Date().getTime(),
      Effect: 'Allow',
      Principal: {
        AWS: '*',
      },
      Action: 'SQS:SendMessage',
      Resource: queueArn,
      Condition: {
        ArnEquals: {
          'aws:SourceArn': topicArn,
        },
      },
    }],
  };

  return new Promise((resolve, reject) => {
    sqs.setQueueAttributes({QueueUrl: queueUrl, Attributes: {Policy: JSON.stringify(attributes)} }, (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(true);
      }
    });
  });
}

/**
 * Write data to config file
 * @returns true if file is created
 * rejects with error
 */

function writeConfigFile() {
  return new Promise((resolve, reject) => {
    fs.writeFile('config.json', JSON.stringify(config, null, 4), err => {
      if (err) {
        reject(err);
      } else {
        resolve(true);
      }
    });
  });
}

/**
 * Delete Queue
 * @param queueUrl queue url (String)
 * @returns true if queue is deleted
 * rejects with error
 */

function deleteQueue(queueUrl) {
  return new Promise((resolve, reject) => {
    sqs.deleteQueue({QueueUrl: queueUrl}, (err, result) => {
      if (err) {
        reject(err);
      } else {
        resolve(true);
      }
    });
  });
}
module.exports.listTopics = listTopics;
module.exports.listAllQueues = listAllQueues;
module.exports.checkIfQueueExists = checkIfQueueExists;
module.exports.createQueue = createQueue;
module.exports.getQueueAttributes = getQueueAttributes;
module.exports.snsSubscribe = snsSubscribe;
module.exports.setQueueAttributes = setQueueAttributes;
module.exports.writeConfigFile = writeConfigFile;
module.exports.deleteQueue = deleteQueue;

