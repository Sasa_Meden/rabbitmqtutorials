const qController = require('./controllers/queueControllers/queues_controller');

const getTopicList = async() => {
  console.log(await qController.listTopics());
  return 'done';
};

getTopicList();
