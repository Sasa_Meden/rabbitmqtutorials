#RabbitMQ Tutorials (No.4 publish/subscribe) and Amazon SQS/SNS publish/subscribe example

**Each in separate folder**

#Pricing

###RabbitMQ/cloudamqp pricing

https://www.cloudamqp.com/plans.html

###Amazon SQS/SNS Price calculators

http://calculator.s3.amazonaws.com/index.html


#aws_sdk-mock

https://www.npmjs.com/package/aws-sdk-mock


###Amazon SQS Free Tier

**You can get started with Amazon SQS for free. All customers can make 1 million Amazon SQS requests for free each month. Some applications might be able to operate within this Free Tier limit.**


###Amazon SNS Free Tier

**Each month, Amazon SNS customers receive 1,000,000 Amazon SNS Requests, 100,000 HTTP notifications, 1,000 email notifications and 100 SMS notifications for free.**


#Best Practices

##Processing Messages in a Timely Manner

###Visibility timeout

**Timeout depends on how long it takes our app to proces and delete message**
**E.g. If processing takes 5 seconds and visibility timeout is set to 5 minutes, if processing for some reason fails you must wait for those 5 minutes to pass before making another attempt at processing.**

**On the other hand if our app takes 5 seconds to process and visibility timeout is set to 1 second, a duplicate message is received by another consumer while the original consumer is still working on the message.**

**Solution: If we know or can reasonably estimate processing time then we should set visibility timeout to maximum time it takes to process. If we don't know how long it takes then  we create "heartbeat for consumer (as long as consumer is working on message we keep extending visibility timeout). E.g.  Specify the initial visibility timeout (for example, 2 minutes) and then�as long as your consumer still works on the message�keep extending the visibility timeout by 2 minutes every minute.**

**Configuring the Visibility Timeout specifics:**

https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-visibility-timeout.html

##Handling Request Errors

**Since we are using AWS SDK, we already have retry and backoff logic that SDK implements automatically. We can configure additional settings for retry with ClientConfiguration class (increase or decrease maxErrorRetry value). Backoff is exponential algorithm for better flow control - it uses progressively longer waits between retries for consecutive error responses. Recomendation is to implement maximum delay interval, as well as a maximum number of retries. Those values are not necessarily fixed, and should be set based on the operation being performed, as well as other local factors, such as network latency.**

**Backoff and Jitter in details**

https://docs.aws.amazon.com/general/latest/gr/api-retries.html

https://aws.amazon.com/blogs/architecture/exponential-backoff-and-jitter/

##Reducing cost of SQS

**By setting up Long Polling we can eliminate the number of empty responses - when there are no messages available for consumer (RecieveMessage request) and when messages are available but not included in a response. By default, SQS uses short polling (WaitTimeSeconds parameter of a ReceiveMessage request is set to 0)**

**In most cases ReceiveMessage wait time can be set to 20 seconds, minimum is 1 second. For multiple queues, if long polling is set we should use separate threads for each queue**

##Dead-Letter queues

**To capture all messages that can't be processed, and to ensure the correctness of CloudWatch metrics, we should configure a dead-letter queue. If source queue fails to process a message a specified number of times the redrive policy redirects messages to dead-letter queue that decreases the number of messages and reduces the possibility of exposing you to "poison pill" messages (messages that are received but can't be processed).**

**Including a "poison pill" message in a source queue can distort the ApproximateAgeOfOldestMessage CloudWatch metric by giving an incorrect age of the "poison pill" message. Configuring a dead-letter queue helps avoid false alarms if/when using this metric.**

**Retention period of a dead-letter queue should always be longer than the retention period of the original queue.**

**If we implement dead-letter queues, maximum number of receivers should not be 1. In some scenarios if ReceiveMessage call fails, a message might be moved to a dead-letter queue without being received (if maximum receivers is set to 1). Dead-letter queues are useful for debugging application or messaging system because they let us isolate problematic messages to determine why their processing doesn't succeed.**

**More details on dead-letter queues:**

https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-dead-letter-queues.html

##Summary

**I wrote this "Best Practices" guidelines not knowing what type of queue you will use - standard queue (async) or fifo, how many messages are expected and what will you do with them (how long will processing of a message last before you delete it), how many producers and consumers on queues? All these questions should be answered for me to give you specific quidelines tailored to your needs. These quidelines are for standard queue (async). SNS does not work with fifo queues so different approach is needed.**