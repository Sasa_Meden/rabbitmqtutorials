#Publish/Subscribe

![](publish_subscribe.png)

##emit_log.js
- **Opens connection to RabbitMQ server**
- **Opens channel**
- **Declares exchange with name and fanout type**
- **Publishes message to named exchange**
- **Closes connection**

##receive_logs.js
- **Opens connection to RabbitMQ server**
- **Opens channel**
- **Declares exchange (same name) and sets type(fanout), durable=false**
- **Creates queue with server generated name (exclusive, once closed queue is deleted)**
- **Binds that queue with our exchange**
- **Consumes (recieves message from that queue)**

**(CTRL+c to close/exit and delete queue)**
**To save logs: console: receive_logs.js > logs_from_rabbit.log**
**To see all bindings and queue names: console: sudo rabbitmqctl list_bindings**


