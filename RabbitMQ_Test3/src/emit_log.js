#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', (err, conn) => {
  conn.createChannel((err, ch) => {
    var exchange = 'logs'; // Name exchange 
    var msg = process.argv.slice(2).join(' ') || 'Some message text';

    ch.assertExchange(exchange, 'fanout', {durable: false}); // Create exchange named logs, type fanout = send to all known queues
    ch.publish(exchange, '', new Buffer(msg)); // second param - empty string: publish to logs exchange, not any particular queue
    console.log(' [x] Sent %s', msg);
  });
  setTimeout(() => { conn.close(); process.exit(0) }, 500);
});