#!/usr/bin/env node

const amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', (err, conn) => {
  conn.createChannel((err, ch) => {
    var exchange = 'logs'; // Name exchange 
    
    ch.assertExchange(exchange, 'fanout', {durable: false});
    // with empty string in assertQueue function server will generate random que name (q.queue)
    // that queue is nondurable so when consumer disconnects queue is deleted (exclusive flag) 
    ch.assertQueue('', {exclusive: true}, (err,q) => {
      console.log(' [*] Waiting for messages in %s. To exit press CTRL+C', q.queue);
      // we are telling our exchange to send message to our queue (binding)
      // fanout type exchange ignores last param.(empty string)
      ch.bindQueue(q.queue, exchange, ''); 
      ch.consume(q.queue, (msg) => {
        console.log(" [x] %s", msg.content.toString());
      }, {noAck: true});
    });
  });
});

// to save logs: console:  receive_logs.js > logs_from_rabbit.log
// call receive_logs.js X times
// to see all bindings: console: sudo rabbitmqctl list_bindings
// you should see X data instances going from our exchange to X server generated queues
