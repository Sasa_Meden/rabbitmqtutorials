// Load the SDK for JavaScript
var AWS = require('aws-sdk');
// Set the region 
AWS.config.update({region: 'us-east-2'});

// Create an SQS service object
const sqs = new AWS.SQS();

const paramsListQueues = {};

const paramsCreateQueues = {
    QueueName: 'created_queue',
    Attributes: {
        'DelaySeconds': '60',
        'MessageRetentionPeriod': '86400'        
    }
};
const paramsGetQueueUrl = {
    QueueName: 'created_queue'
};
const paramsDeleteQueue = {
    QueueUrl: ''
};

lstQueues = () => {
    sqs.listQueues(paramsListQueues, (err, data) => {
        if (err) {
          console.log("Error", err);
        } else {
          console.log("SuccessLISTQ", data);
          console.log("Success", data.QueueUrls);
        }
      });
}

crtQueue = () => {
    sqs.createQueue(paramsCreateQueues, (err, data) => {
        if (err) {
          console.log("Error", err);
        } else {
          console.log("SuccessCRTQ", data);
         console.log("Success", data.QueueUrl);
        }
      });
}

getUrlQueue = () => {
    sqs.getQueueUrl(paramsGetQueueUrl, (err, data) => {
        if (err) {
          console.log("Error", err);
        } else {
          console.log("SuccessGETURL", data);
          console.log("Success", data.QueueUrl);
          paramsDeleteQueue.QueueUrl = data.QueueUrl;
        }
      });
}
queueDelete = () => {
    sqs.deleteQueue(paramsDeleteQueue, (err, data) => {
        if (err) {
            console.log("Error", err);
          } else {
            console.log("SuccessDELETEQ", data);
            
          }
      });
}

lstQueues();
//crtQueue();
//setTimeout(lstQueues,5000);
/*setTimeout(getUrlQueue,5000);
queueDelete();
setTimeout(lstQueues,5000);
crtQueue();
setTimeout(lstQueues,5000);*/