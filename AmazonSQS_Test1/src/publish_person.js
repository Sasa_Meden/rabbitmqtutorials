var AWS = require('aws-sdk'); 
var util = require('util');
var config = require('./config.json');

// configure AWS
AWS.config.update({
  'region': 'us-east-2'
});

var sns = new AWS.SNS();

function publish(mesg) {
  var publishParams = { 
    TopicArn : config.TopicArn,
    Message: mesg,
    MessageAttributes : {
      "Person" : {
        "DataType":"String",
        "StringValue":"Steve"
      }
    }
  };

  sns.publish(publishParams, function(err, data) {
    console.log("sent: " + publishParams.Message + "\n " + 
    JSON.stringify(publishParams.MessageAttributes) + "\n");
  });
}
//setTimeout(publish, 1000);
for (var i=0; i < 1; i++) {
  publish("Person Message " + i);
}

