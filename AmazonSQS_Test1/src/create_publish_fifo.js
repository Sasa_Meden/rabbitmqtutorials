var AWS = require('aws-sdk'); 
var util = require('util');
var config = require('./config.json');
var sqsFifo = require("sqs-fifo")
// configure AWS
AWS.config.update({
  'region': 'us-east-2'
});

const queue = new sqsFifo({
    accessKeyId: AWS.config.accessKeyId,
    secretAccessKey: AWS.config.secretAccessKey,
    name: "test_que.fifo"
  })

var msg = {
  "Txt": "some text"
}

sendToQue = (msg) => {

  //for (var i=0; i < 10; i++) {
    msg.Txt = msg.Txt;// + " " + i;

    queue.push({
      some: msg,
      stringifable: 'to json'
    }).then(()=> {
      // successfully put in the queue
      console.log("success");
    }, e=> {
      // error if we failed
      console.log("fail: " + e);
    })
 // }
}

sendToQue(msg);

