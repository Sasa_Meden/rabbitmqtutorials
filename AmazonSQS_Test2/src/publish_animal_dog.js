var AWS = require('aws-sdk'); 
var util = require('util');
var config = require('./config.json');

// configure AWS
AWS.config.update({
  'region': 'us-east-2'
});

var sns = new AWS.SNS();

function publish(mesg) {
  var publishParams = { 
    TopicArn : config.TopicArn,
    Message: mesg,
    MessageAttributes : {
      "Animal" : {
        "DataType":"String",
        "StringValue":"Dog"
      }
    }
  };

  sns.publish(publishParams, function(err, data) {
    console.log("sent: " + publishParams.Message + "\n " + 
    JSON.stringify(publishParams.MessageAttributes) + "\n");
  });
}
//setTimeout(publish, 1000);
for (var i=0; i < 10; i++) {
  publish("Animal Message " + i);
}

