var AWS = require('aws-sdk'); 
var util = require('util');
var config = require('./config.json');

// configure AWS
AWS.config.update({
  'region': 'us-east-2'
});

var sns = new AWS.SNS();

function publish(mesg) {
  var publishParams = { 
    TopicArn : config.TopicArn,
    Message: mesg,
  };

  sns.publish(publishParams, function(err, data) {
    console.log("sent: " + publishParams.Message + "\n ");
  });
}
//setTimeout(publish, 1000);
for (var i=0; i < 10; i++) {
  publish("General Message " + i);
}

