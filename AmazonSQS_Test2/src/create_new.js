var AWS = require('aws-sdk');
var util = require('util');
var async = require('async');
var fs = require('fs');
var cmd=require('node-cmd');
// configure AWS
AWS.config.update({
  'region': 'us-east-2'
});

var sns = new AWS.SNS();
var sqs = new AWS.SQS();

var config = {};
config.TopicArn = "arn:aws:sns:us-east-2:719738972379:message_topic";
var queue_name = 'standard_queue1';
//var paramsListQueues = {};
var awsQueues = [];
var noQueue = true;
var paramsGetQueueUrl = {
  QueueName: 'queue_test'
}

//Check if queue with name exists
function listQueues(cb) {
  sqs.listQueues({}, function (err, result) {
    if (err !== null) {
      console.log(util.inspect(err));
      return cb(err);
    }

    // console.log(util.inspect(result));

    awsQueues = result.QueueUrls;

    cb();
  });
}

function checkIfQueueExists(cb) {
  for (var i = 0; i < awsQueues.length; i++) {
    if (awsQueues[i].indexOf(queue_name) != -1) {
      console.log("Queue with that name already exists. Try with another name.");
      return cb("Queue with that name already exists. Try with another name.");
    }
  }
  paramsGetQueueUrl.QueueName = queue_name;

  //  console.log(util.inspect(paramsGetQueueUrl));

  cb();
}

function createQueue(cb) {
  sqs.createQueue({
    'QueueName': queue_name
  }, function (err, result) {

    if (err !== null) {
      console.log(util.inspect(err));
      return cb(err);
    }

    // console.log(util.inspect(result));

    config.QueueUrl = result.QueueUrl;

    cb();
  });

}

function getQueueAttr(cb) {
  sqs.getQueueAttributes({
    QueueUrl: config.QueueUrl,
    AttributeNames: ["QueueArn"]
  }, function (err, result) {

    if (err !== null) {
      console.log(util.inspect(err));
      return cb(err);
    }
    // console.log(util.inspect(result));

    config.QueueArn = result.Attributes.QueueArn;

    cb();

  });

}


function snsSubscribe(cb) {
  sns.subscribe({
    'TopicArn': config.TopicArn,
    'Protocol': 'sqs',
    'Endpoint': config.QueueArn,
  }, function (err, result) {

    if (err !== null) {
      console.log(util.inspect(err));
      return cb(err);
    }
    //console.log(util.inspect(result));
    cb();
  });
}

function setQueueAttr(cb) {

  var queueUrl = config.QueueUrl;
  var topicArn = config.TopicArn;
  var sqsArn = config.QueueArn;

  var attributes = {
    "Version": "2008-10-17",
    "Id": sqsArn + "/SQSDefaultPolicy",
    "Statement": [{
      "Sid": "Sid" + new Date().getTime(),
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": "SQS:SendMessage",
      "Resource": sqsArn,
      "Condition": {
        "ArnEquals": {
          "aws:SourceArn": topicArn
        }
      }
    }]
  };

  sqs.setQueueAttributes({
    QueueUrl: queueUrl,
    Attributes: {
      'Policy': JSON.stringify(attributes)
    }
  }, function (err, result) {

    if (err !== null) {
      console.log(util.inspect(err));
      return cb(err);
    }

    // console.log(util.inspect(result));

    cb();
  });

}

function writeConfigFile(cb) {
  fs.writeFile('config.json', JSON.stringify(config, null, 4), function (err) {
    if (err) {
      return cb(err);
    }
    console.log(queue_name + " queue created, configuration saved to config.json");
    cb();
  });
}

/*function startConsume(cb) {
  try{
    cmd.run('node consume_all.js');
  }catch(e){
    return cb(e);
  }
  cb();
}

fs.stat('config.json', function (err, stat) {
  if (err == null) {
    console.log('File exists');
    //Start consume script here
    cmd.run('node consume_all.js');
  } else if (err.code == 'ENOENT') {
    // file does not exist
    async.series([listQueues, checkIfQueueExists, createQueue, getQueueAttr, snsSubscribe, setQueueAttr, writeConfigFile,startConsume]);
  } else {
    async.series([listQueues, checkIfQueueExists, createQueue, getQueueAttr, snsSubscribe, setQueueAttr, writeConfigFile,startConsume]);
  }
});*/



async.series([listQueues, checkIfQueueExists, createQueue, getQueueAttr, snsSubscribe, setQueueAttr, writeConfigFile]);
