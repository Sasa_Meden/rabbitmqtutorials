var AWS = require('aws-sdk');
var util = require('util');
var config = require('./config.json');

// configure AWS
AWS.config.update({
  'region': 'us-east-2'
});

var sqs = new AWS.SQS();

var receiveMessageParams = {
  QueueUrl: config.QueueUrl,
  MaxNumberOfMessages: 10
};

getMessages = () => {
  sqs.receiveMessage(receiveMessageParams, receiveMessageCallback);
}

receiveMessageCallback = (err, data) => {
  if (data && data.Messages && data.Messages.length > 0) {
    for (var i = 0; i < data.Messages.length; i++) {
      console.log(JSON.parse(data.Messages[i].Body).Message);
      // Delete the message when we've successfully processed it
      var deleteMessageParams = {
        QueueUrl: config.QueueUrl,
        ReceiptHandle: data.Messages[i].ReceiptHandle
      };
      sqs.deleteMessage(deleteMessageParams, deleteMessageCallback);
    }
    getMessages();
  } else {
    //process.exit(1);
    setTimeout(getMessages, 1000);
  }
}
deleteMessageCallback = (err, data) => {
  //console.log("deleted message");
  //console.log(data);
}

setTimeout(getMessages, 1000);
