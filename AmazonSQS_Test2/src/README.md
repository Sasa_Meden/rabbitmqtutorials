#SNS/SQS Example

This is an example of how to use AWS SNS and SQS to publish messages to a topic(SNS) and consume them from a queue(SQS) using NodeJS and the aws-sdk.

##Prep
Log into your AWS console and create a new user in IAM. Make sure you save the users credientials. 
Attach the User Policies for Amazon SQS Full Access and Amazon SNS Full Access.  

Create ~/.aws/credentials

Add the access key and secret access key for the IAM user you just created.
```
[snssqs]
aws_access_key_id = <YOUR_ACCESS_KEY_ID>
aws_secret_access_key = <YOUR_SECRET_ACCESS_KEY>
```

##Install Packages

````
npm install
````

##Create Topic and Queue
http://budiirawan.com/how-to-connect-amazon-sns-to-amazon-sqs-queues/

##Pricing/Queue types 
https://aws.amazon.com/sqs/pricing/

###Amazon SQS Free Tier

**You can get started with Amazon SQS for free. All customers can make 1 million Amazon SQS requests for free each month. Some applications might be able to operate within this Free Tier limit.**


###Amazon SNS Free Tier

**Each month, Amazon SNS customers receive 1,000,000 Amazon SNS Requests, 100,000 HTTP notifications, 1,000 email notifications and 100 SMS notifications for free.**

##Pricing calculators
http://calculator.s3.amazonaws.com/index.html

**In this example I'm using Standard Queue**